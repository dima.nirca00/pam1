import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

final Color mainHexColor = HexColor("#1C1C1D");
final Color accentHexColor = HexColor("#2CC91C");
